package com.cjzl.cjzlpro;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CjzlproApplication {

    public static void main(String[] args) {
        SpringApplication.run(CjzlproApplication.class, args);
    }

}

